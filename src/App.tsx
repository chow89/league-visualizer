import React from 'react';
import './App.css';
import { Team as T } from './enums/team';
import { Team } from './model/team';
import { fixtures, FixtureData } from './data/fixtures';
import Table from './components/table';

type AppState = {
  teams: Array<Team>
};

export default class App extends React.Component<{}, AppState> {
  constructor(props: {}) {
    super(props);
    const teams = [
      new Team(T.OSNABRUCK, 'VfL'),
      new Team(T.KARLSRUHER_SC, 'KSC'),
      new Team(T.WEHEN_WIESBADEN, 'SVWW'),
      new Team(T.HALLESCHER_FC, 'HFC'),
      new Team(T.WURZBURGER_KICKERS, 'FWK'),
      new Team(T.HANSA_ROSTOCK, 'FCH'),
      new Team(T.PREUSSEN_MUNSTER, 'SCP'),
      new Team(T.FSV_ZWICKAU, 'FSV'),
      new Team(T.KAISERSLAUTERN, '1.FCK'),
      new Team(T.KFC_UERDINGEN, 'KFC'),
      new Team(T.TSV_MUNCHEN, '1860'),
      new Team(T.SV_MEPPEN, 'SVM'),
      new Team(T.SPVGG_UNTERHACHING, 'SpVgg'),
      new Team(T.EINTRACHT_BRAUNSCHWEIG, 'BTSV'),
      new Team(T.ENERGIE_COTTBUS, 'FCE'),
      new Team(T.CARL_ZEISS_JENA, 'FCCZ'),
      new Team(T.SONNENHOF_GROSSASPACH, 'SGS'),
      new Team(T.SPORTFREUNDE_LOTTE, 'SFL'),
      new Team(T.FORTUNA_KOLN, 'SCF'),
      new Team(T.VFR_AALEN, 'VfR')
    ].map((t: Team) => this.findFixturesForTeam(t))
      .sort((t1: Team, t2: Team) => this.sortTeams(t1, t2));
      
    this.state = {
      teams: teams
    };
    window.console.log(teams);
  }

  private findFixturesForTeam(t: Team): Team {
    t.setFixtures(fixtures.filter((fixture: FixtureData) => fixture[0] === t.key || fixture[1] === t.key))
    return t;
  }

  private sortTeams(t1: Team, t2: Team): number {
    if (t1.points === t2.points) {
      return this.sortByTotalGoals(t1, t2);
    }
    return t2.points - t1.points;
  }

  private sortByTotalGoals(t1: Team, t2: Team): number {
    return (t2.goals[0] - t2.goals[1]) - (t1.goals[0] - t1.goals[1]);
  }

  render() {
    return (
      <div className="App">
        <Table teams={this.state.teams}/>
      </div>
    );
  }
}

type TeamFixtures = {
  team: Team,
  fixtures: Array<FixtureData>
}

type RankingParameters = {
  team: Team,
  points: number,
  goals: [number, number]
}