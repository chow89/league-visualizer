import React from 'react';
import { Team } from '../model/team';
import TeamColumn from './teamcolumn';

type TableProps = {
  teams: Array<Team>
};

type TableState = {};

export default class Table extends React.Component<TableProps, TableState> {

  private renderTeamColumns(): Array<JSX.Element> {
    return this.props.teams.map((t: Team, i: number) => <TeamColumn teams={this.props.teams} team={t} key={i}/>)
  }

  render() {
    return (
      <div className="flex">
        <div className="flex">
          <div className="title">
            3. Liga
          </div>
          <div className="colour-legend flex">
            <div className="flex">
              <div className="game green"/>
              <div>Sieg</div>
            </div>
            <div className="flex">
              <div className="game yellow"/>
              <div>Unentschieden</div>
            </div>
            <div className="flex">
              <div className="game red"/>
              <div>Niederlage</div>
            </div>
          </div>
        </div>
        <div className="flex">
          {this.renderTeamColumns()}
        </div>
      </div>
    );
  }
}