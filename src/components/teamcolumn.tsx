import React from 'react';
import { Team } from '../model/team';
import { FixtureData } from '../data/fixtures';

type TeamColumnProps = {
  team: Team,
  teams: Array<Team>
};

type TeamColumnState = {};

export default class TeamColumn extends React.Component<TeamColumnProps, TeamColumnState> {
  
  private renderGames(): Array<JSX.Element> {
    return this.props.teams.map((opponent: Team) => {
      if (opponent.key === this.props.team.key) {
        return (
          <div className="team black center">
            {this.props.team.shortCode}
          </div>
        );
      }
      const fixtures = this.findFixtures(opponent);
      return (
        <div className="flex">
          <div>
            {this.renderHomeGame(fixtures[0])}
          </div>
          <div>
            {this.renderAwayGame(fixtures[1])}
          </div>
        </div>
      );
    });
  }

  private renderHomeGame(fixture: FixtureData): JSX.Element {
    if (!fixture || fixture[2] === undefined || fixture[3] === undefined) {
      return <div className="game white"/>
    }
    if (fixture[2] > fixture[3]) {
      return <div className="game green"/>;
    } else if (fixture[2] === fixture[3]) {
      return <div className="game yellow"/>;
    } else {
      return <div className="game red"/>;
    }
  }

  private renderAwayGame(fixture: FixtureData): JSX.Element {
    if (!fixture || fixture[2] === undefined || fixture[3] === undefined) {
      return <div className="game white"/>
    }
    if (fixture[3] > fixture[2]) {
      return <div className="game green"/>;
    } else if (fixture[3] === fixture[2]) {
      return <div className="game yellow"/>;
    } else {
      return <div className="game red"/>;
    }
  }

  private findFixtures(opponent: Team): Array<FixtureData> {
    return this.props.team.getFixtures()
    .filter((f: FixtureData) => f[0] === opponent.key || f[1] === opponent.key)
    .sort((f1, f2) => f1[0] === this.props.team.key ? -1 : 1);  // set home game on 0, and away game on 1
  }
  
  render() {
    return (
      <div>
        <div className="flex teamcolumn column">
          {this.renderGames()}
        </div>
        <div className="flex col-legend">
          <div className="center col-legend-text">Heim</div>
          <div className="center col-legend-text">Auswärts</div>
        </div>
      </div>
    );
  }
}