export enum MatchStatus {
    PLANNED, HOMEWIN, DRAW, AWAYWIN
}