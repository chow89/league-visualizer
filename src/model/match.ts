import { Team } from "../enums/team";
import { MatchStatus } from "../enums/matchstatus";

export class Match {
    private status: MatchStatus;
    constructor(private hometeam: Team, private awayteam: Team, private homegoals?: number, private awaygoals?: number) {
        if (homegoals !== undefined && awaygoals !== undefined) {
            this.status = homegoals > awaygoals ? MatchStatus.HOMEWIN : homegoals < awaygoals ? MatchStatus.AWAYWIN : MatchStatus.DRAW;
        } else {
            this.status = MatchStatus.PLANNED
        }
    }
}