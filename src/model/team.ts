import { Team as T } from '../enums/team';
import { FixtureData, HomeGoals, AwayGoals } from '../data/fixtures';

export class Team {
    private fixtures: Array<FixtureData> = [];
    private totalPoints: number = 0;
    private totalGoals: [HomeGoals, AwayGoals] = [0, 0];
    constructor(private team: T, private name: string) {}

    public get key(): T {
        return this.team;
    }

    public get shortCode(): string {
        return this.name;
    }

    public addFixture(f: FixtureData) {
        this.fixtures.push(f);
    }

    public setFixtures(fixtures: Array<FixtureData>) {
        this.fixtures = fixtures;
    }

    public getFixtures(): Array<FixtureData> {
        return this.fixtures;
    }

    public get points() {
        if (!this.totalPoints) {
            this.calculateTotalPoints();
        }
        return this.totalPoints;
    }

    public get goals() {
        if (this.totalGoals[0] === 0 && this.totalGoals[1] === 0) {
            this.calculateTotalGoals();
        }
        return this.totalGoals;
    }

    private calculateTotalPoints() {
        this.totalPoints = this.fixtures.reduce((totalPoints: number, fixture: FixtureData) => totalPoints + this.getPointsPerFixture(fixture), 0)
    }

    private getPointsPerFixture(fixture: FixtureData) {
        if (fixture[2] === undefined || fixture[3] === undefined) {
            return 0;
        }
        if (this.isHomeWin(fixture) || this.isAwayWin(fixture)) {
            return 3;
        } else if (fixture[2] === fixture[3]) {
            return 1;
        }
        return 0;
    }

    private isHomeWin(fixture: FixtureData) {
        return fixture[2]! > fixture[3]! && fixture[0] === this.key;
    }

    private isAwayWin(fixture: FixtureData) {
        return fixture[2]! < fixture[3]! && fixture[1] === this.key;
    }

    private calculateTotalGoals() {
        const goalsAtHome = this.fixtures.filter((f: FixtureData) => f[0] === this.key).reduce((totalGoals: [HomeGoals, AwayGoals], fixture: FixtureData) => [totalGoals[0] + (fixture[2] ? fixture[2] : 0), totalGoals[1] + (fixture[3] ? fixture[3] : 0)], [0, 0]);
        const goalsAway = this.fixtures.filter((f: FixtureData) => f[1] === this.key).reduce((totalGoals: [HomeGoals, AwayGoals], fixture: FixtureData) => [totalGoals[0] + (fixture[3] ? fixture[3] : 0), totalGoals[1] + (fixture[2] ? fixture[2] : 0)], [0, 0]);
        this.totalGoals = [goalsAtHome[0] + goalsAway[0], goalsAtHome[1] + goalsAway[1]];
    }
}